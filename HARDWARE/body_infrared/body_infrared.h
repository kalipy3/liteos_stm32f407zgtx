/*
 * body_infrared.h
 * Copyright (C) 2020 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef BODY_INFRARED_H
#define BODY_INFRARED_H

extern int infrared_flag;
void drv_body_infrared_init(void);
//人体红外感应task
int body_infrared_task_start(void);

#endif /* !BODY_INFRARED_H */
