/*
 * body_infrared.c
 * Copyright (C) 2020 2020-12-25 22:57 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#include "body_infrared.h"
#include "usart3.h"
#include "los_sys.h"
#include "los_task.h"
#include "los_memory.h"
#include "los_base.h"
#include "main.h"

int infrared_flag = 0;
void drv_body_infrared_init(void)
{

    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOG_CLK_ENABLE();

    /*Configure GPIO pin : PG1 */
    //pg1作为红外电平的输入
    GPIO_InitStruct.Pin = GPIO_PIN_1;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOG, GPIO_PIN_1, GPIO_PIN_RESET);
}

//人体红外感应task
void body_infrared_Task(void *arg)
{
    LOS_TaskDelay(15000);
    while (1)
    {
        LOS_TaskDelay(20);
        if (HAL_GPIO_ReadPin(GPIOG, GPIO_PIN_1) == GPIO_PIN_SET && infrared_flag == 0)
        {
            infrared_flag = 1;
            u3_printf("drv_body_infrared gpio set..");

            //Take a picture with your Android phone of remote by esp8266
            //why not the camera module of STM32,because I don't have enough money,so I chose to use ADB to call android's camera indirectly through wifi
            nodeserver_doGet_msg_recive_and_paser_task_start();//start a liteos task
        }
    }
}

int body_infrared_task_start(void)
{
    UINT32 uwRet = 0;
    UINT32 uwTask;
    TSK_INIT_PARAM_S stInitParam;

    stInitParam.pfnTaskEntry = body_infrared_Task;
    stInitParam.usTaskPrio = 9;
    stInitParam.uwStackSize = 0x400;
    stInitParam.pcName = "infrared";

    drv_body_infrared_init();

    u3_printf("body_infrared_Task create...\r\n");
    uwRet = LOS_TaskCreate(&uwTask, &stInitParam);
    if(uwRet != LOS_OK)
    {
        u3_printf("body_infrared_Task create failed\r\n");
        return uwRet;
    }
    u3_printf("body_infrared_Task create ok\r\n");
}
