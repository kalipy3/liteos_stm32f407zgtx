/*
 * node_http.c
 * Copyright (C) 2020 2020-12-19 20:14 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#include "node_http.h"

#include "stm32f4xx.h" 
#include "stdio.h"
#include "stdarg.h"
#include "string.h"
#include "usart3.h"//u3_printf()
#include "wifi.h"//extern:Connect_flag
#include "main.h"//extern:r_flag和s_flag和g_doGetMux
#include "uart4.h"//extern:WiFi_RxCounter和WiFi_RX_BUF和WiFi_RXBUFF_SIZE
#include "onenet_http.h"//extern char RXbuff[2048];
#include "led.h"
#include "beep.h"
#include "los_sys.h"
#include "los_task.h"
#include "los_mux.h"
#include "los_base.h"
#include "body_infrared.h"

char * node_server_ip = "192.168.43.166";
int node_server_port = 3000;

char TXbuff[2048];//send buf

//GET请求 /takePicture
//服务器返回的数据在esp8266的串口寄存器，请自行编写中断接收，推荐用中断+操作系统的任务来处理数据
void nodeserver_take_picture_doGet(char *resp, int timeout)
{
    UINT32 uwRet;

    /*申请互斥锁*/
    uwRet=LOS_MuxPend(g_doGetMux, LOS_WAIT_FOREVER);
    if(uwRet == LOS_OK)
    {
        u3_printf("ready link to node_server\r\n");
        if(esp8266_connect_server(node_server_ip, node_server_port, timeout)){
            Connect_flag = 0;                //连接成功标志清除
            r_flag = s_flag = 0;             //标志清除
            u3_printf("link to node_server fail\r\n"); //串口提示数据
        }else{                               //返回0，进入else分支，表示连接服务器成功
            u3_printf("link to node_server ok\r\n"); //串口提示数据
            WiFi_RxCounter=0;                           //WiFi接收数据量变量清零                        
            memset(WiFi_RX_BUF,0,WiFi_RXBUFF_SIZE);     //清空WiFi接收缓冲区 
            Connect_flag = 1;                //连接成功标志置位

            //构造http请求报文
            memset(TXbuff,0,2048);   //清空缓冲区
            sprintf(TXbuff,"GET /takePicture HTTP/1.0\r\n");//构建报文
            strcat(TXbuff,"\r\n\r\n");                     //追加报文

            WiFi_printf(TXbuff);             //把构建好的报文发给服务器
            u3_printf("nodeserver_doGet baowen sended, now waiting for response..\r\n");

            while (Usart4_RxCompleted == 0);//等待 写成Usart4_RxCompleted != 1的形式会阻塞在while内部

            Usart4_RxCompleted = 0;//清除Usart4_RxCompleted置位标志                                         
            u3_printf("esp8266_received_data:%s\r\n", &RXbuff[2]);

            memcpy(resp, (char *)RXbuff, strlen(RXbuff)+1);
            u3_printf("resp of nodeserver:%s\r\n", resp);
            WiFi_Close(50);//准备关闭连接，超时单位100ms，超时时间5s
            u3_printf("close connect with nodeserver ok\r\n");
        }

        /*释放互斥锁*/
        LOS_MuxPost(g_doGetMux);
        u3_printf("esp8266_take_picture_doGet()---g_doGetMux released..\r\n");
    }
}

//nodeserver接收报文解析
void nodeserver_recive_msg_paser(char *resp)
{
    char *presult;

    if(strstr(&resp[2],"200 OK"))//搜索200 OK 表示报文正确，进入if
    {                                	
        u3_printf("nodeserver http 200 ok\r\n");
        u3_printf("nodeserver_recive_msg_paser..\r\n");//串口提示数据
        if(strstr(&resp[2],"\"error_code\":0"))//搜索"error_code":0 表示数据流操作正确，进入if
        {                       
            if(strstr(&resp[2],"score"))//搜索"score",score大于80说明识别成功,score是百度人脸识别api返回的相似度,取值为[0,100]
            {                   
                presult = strstr(&resp[2],"\"score\":"); //搜索 "score": 
                if(presult!=NULL)//如果搜索到了，进入if
                {                  
                    u3_printf("nodeserver_recive_msg_paser presult:%s\r\n", presult);
                    //根据u3_printf打印的信息，可知presult[8]和[9]保存了score的整数部分，注意：当score为100的时候，我们认为其是作假的，当成score<80来处理
                    u3_printf("presult[8]:%c\r\n", presult[8]);
                    u3_printf("presult[9]:%c\r\n", presult[9]);
                    if(presult[8]>='8' && presult[9]>='0')//如果score>=80 打开LED1 否则，闪烁LED1和beep报警3个led闪烁周期
                        drv_led_on(led1);     
                    else {
                        drv_beep_on();
                        drv_liteos_led_flash(led1, 500);
                        drv_liteos_led_flash(led1, 500);
                        drv_liteos_led_flash(led1, 500);
                        drv_beep_off();
                    }
                }
            }
        }
        u3_printf("nodeserver_recive_msg_paser finished\r\n");
    } 
    else {
        u3_printf("nodeserver recive baowen error!\r\n");
        u3_printf("error baowen data:%s\r\n",&resp[2]);
    }
    infrared_flag = 0;
}
