/*
 * node_http.h
 * Copyright (C) 2020 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef NODE_HTTP_H
#define NODE_HTTP_H

extern char * node_server_ip;
extern int node_server_port;

//请求node服务器 请求路径:/takePicture
//服务器返回的数据在esp8266的串口寄存器，请自行编写中断接收，推荐用中断+操作系统的任务来处理数据
void nodeserver_take_picture_doGet(char *resp, int timeout);
            
//nodeserver接收报文解析
void nodeserver_recive_msg_paser(char *resp);

#endif /* !ESP8266_COMMON_H */
