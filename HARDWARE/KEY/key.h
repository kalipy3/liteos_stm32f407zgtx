/*
 * key.h
 * Copyright (C) 2020 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef KEY_H
#define KEY_H

#define key0 0
#define key1 1 
#define key2 2
#define key3 3

#define key0_pin 9 
#define key1_pin 8 
#define key2_pin 7
#define key3_pin 6

#define KEY0 drv_get_key_pin_value(9)
#define KEY1 drv_get_key_pin_value(8)
#define KEY2 drv_get_key_pin_value(7)
#define KEY3 drv_get_key_pin_value(6)

int drv_get_key_pin_value(int keyx_pin);
void drv_key_init(void);
int drv_get_key(void);//不支持连续按
int drv_get_key_hold(void);//支持连续按

#endif /* !KEY_H */
