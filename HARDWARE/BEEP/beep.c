/*
 * beep.c
 * Copyright (C) 2020 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#include "beep.h"
#include "main.h"
#include <string.h>

void drv_beep_init(void)
{
    //使能gpiog
    RCC->AHB1ENR |= 1<<6;

    //GPIOG.7 beep
    GPIOG->MODER &= ~(3<<(7*2));//清零
    GPIOG->MODER |= 1<<(7*2);//通用输出模式
    GPIOG->OTYPER &= ~(1<<7);//清零
    GPIOG->OTYPER |= (0<<7);//推挽输出
    GPIOG->ODR |= 0<<7;//默认beep关闭
}

void drv_beep_on(void)
{
    GPIOG->ODR |= 1<<7;
}

void drv_beep_off(void)
{
    GPIOG->ODR &= ~(1<<7);
}

void drv_beep_toggle(void)
{
    GPIOG->ODR &= (0x0000ffff);//清除高16位
    //GPIOG.7 beep
    if ((GPIOG->ODR & (1<<7)) == 0) {
        drv_beep_on();
    } else {
        drv_beep_off();
    }
}


//beep usart receive msg handler
void drv_beep_usart_receive_msg_handler(char *receive_str)
{
    if(strcmp("open_beep",receive_str)==0) drv_beep_on();
    else if(strcmp("close_beep",receive_str)==0) drv_beep_off();
}

