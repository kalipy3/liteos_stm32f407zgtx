/*
 * beep.h
 * Copyright (C) 2020 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef BEEP_H
#define BEEP_H

void drv_beep_init(void);
void drv_beep_on(void);
void drv_beep_off(void);
void drv_beep_toggle(void);
//beep usart receive msg handler
void drv_beep_usart_receive_msg_handler(char *receive_str);

#endif /* !BEEP_H */
