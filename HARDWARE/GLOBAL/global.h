/*
 * global.h
 * Copyright (C) 2020 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef GLOBAL_H
#define GLOBAL_H

#define g_key_count 4

extern int g_key_status[g_key_count];//0表示没按下 1表示被按下
extern int g_beep_status;//0关闭 1开启

#endif /* !GLOBAL_H */
