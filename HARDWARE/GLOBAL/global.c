/*
 * global.c
 * Copyright (C) 2020 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#include "global.h"

int g_key_status[g_key_count] = {0};//0表示没按下 1表示被按下
int g_beep_status = 0;//0关闭 1开启
