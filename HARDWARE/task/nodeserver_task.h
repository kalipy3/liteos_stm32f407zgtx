/*
 * nodeserver_task.h
 * Copyright (C) 2020 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef NODESERVER_TASK_H
#define NODESERVER_TASK_H

void nodeserver_doGet_msg_recive_and_paser_Task(void *arg);

//自删除的一次性任务
int nodeserver_doGet_msg_recive_and_paser_task_start(void);

#endif /* !NODESERVER_TASK_H */
