/*
 * nodeserver_task.c
 * Copyright (C) 2020 2020-12-21 14:35 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#include "nodeserver_task.h"
#include "los_sys.h"
#include "los_task.h"
#include "los_memory.h"
#include "los_base.h"
#include "node_http.h"
#include "usart3.h"

void nodeserver_doGet_msg_recive_and_paser_Task(void *arg)
{
    static char resp[2048];
    nodeserver_take_picture_doGet(resp, 5000);
    nodeserver_recive_msg_paser((char *)resp);
}

int nodeserver_doGet_msg_recive_and_paser_task_start(void)
{
    UINT32 uwRet = 0;
    UINT32 uwTask;
    TSK_INIT_PARAM_S stInitParam;

    stInitParam.pfnTaskEntry = nodeserver_doGet_msg_recive_and_paser_Task;
    stInitParam.usTaskPrio = 9;
    stInitParam.uwStackSize = 0x400;
    stInitParam.pcName = "deGet";
    //stInitParam.uwResved = LOS_TASK_STATUS_DETACHED;//自删除任务，运行完一次，即删除
    uwRet = LOS_TaskCreate(&uwTask, &stInitParam);
    if(uwRet != LOS_OK)
    {
        u3_printf("nodeserver_doGet_msg_recive_and_paser_Task create failed\r\n");
        return uwRet;
    }
    u3_printf("nodeserver_doGet_msg_recive_and_paser_Task create ok\r\n");
}
