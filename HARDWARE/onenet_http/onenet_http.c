#include "stm32f4xx.h"    
#include "stdio.h"        
#include "stdarg.h"		   
#include "string.h"       
#include "led.h"       
#include "usart3.h"//u3_printf()
#include "wifi.h"//extern:Connect_flag
#include "main.h"//extern:r_flag和s_flag和g_doGetMux
#include "uart4.h"//extern:WiFi_RxCounter和WiFi_RX_BUF和WiFi_RXBUFF_SIZE
#include "onenet_http.h"//extern char RXbuff[2048];
#include "los_sys.h"
#include "los_task.h"
#include "los_mux.h"
#include "los_base.h"

char * onenet_server_ip = "183.230.40.33";
int onenet_server_port = 80;

char RXbuff[2048];                         //接收数据缓冲区
char TXbuff[2048];                         //发送数据缓冲区

//服务器返回的数据在esp8266的串口寄存器，请自行编写中断接收，推荐用中断+操作系统的任务来处理数据
//char *resp参数用来接收onenetserver的响应结果，doGet会把请求结果保存到resp里，因为c语言无法真正的返回一个数组,所以这里使用这种常用的方式
void esp8266_onenet_doGet(char *device_id, char *resp, int timeout)
{
    UINT32 uwRet;

    /*申请互斥锁*/
    uwRet=LOS_MuxPend(g_doGetMux, LOS_WAIT_FOREVER);
    if(uwRet == LOS_OK)
    {
        char temp[128];

        u3_printf("ready link to onenet_server\r\n");
        if(esp8266_connect_server(onenet_server_ip, onenet_server_port, timeout)){
            Connect_flag = 0;                //连接成功标志清除
            r_flag = s_flag = 0;             //标志清除
            u3_printf("link to onenet_server fail\r\n"); //串口提示数据
        }else
        {                               //返回0，进入else分支，表示连接服务器成功
            u3_printf("link to onenet_server ok\r\n"); //串口提示数据
            WiFi_RxCounter=0;                           //WiFi接收数据量变量清零                        
            memset(WiFi_RX_BUF,0,WiFi_RXBUFF_SIZE);     //清空WiFi接收缓冲区 
            Connect_flag = 1;                //连接成功标志置位

            //构造onenet的http请求报文
            memset(TXbuff,0,2048);   //清空缓冲区
            memset(temp,0,128);      //清空缓冲区                                             
            sprintf(TXbuff,"GET /devices/%s/datapoints HTTP/1.1\r\n",device_id);//构建报文
            sprintf(temp,"api-key:%s\r\n",API_KEY);                             //构建报文
            strcat(TXbuff,temp);                                                //追加报文
            strcat(TXbuff,"Host:api.heclouds.com\r\n\r\n");                     //追加报文

            WiFi_printf(TXbuff);             //把构建好的报文发给服务器
            u3_printf("onenet_doGet baowen had send\r\n");

            while (Usart4_RxCompleted == 0);//等待 写成Usart4_RxCompleted != 1的形式会阻塞在while内部

            Usart4_RxCompleted = 0;//清除Usart4_RxCompleted置位标志                                         
            u3_printf("esp8266_received_data:%s\r\n", &RXbuff[2]);
            //for(int i = 0; i < 2048; i++)//也ok
            //{
            //    resp[i] = RXbuff[i];
            //}
            memcpy(resp, (char *)RXbuff, strlen(RXbuff)+1);
            u3_printf("esp8266_onenet_doGet()-->resp of onenet:%s\r\n", resp);
            WiFi_Close(50);//准备关闭连接，超时单位100ms，超时时间5s
            u3_printf("close connect with onenetserver ok\r\n");
        }

        /*释放互斥锁*/
        LOS_MuxPost(g_doGetMux);
        u3_printf("esp8266_onenet_doGet()---g_doGetMux released..\r\n");
    }
}

//onenet接收报文解析
void onenet_recive_msg_paser(char * resp)
{
    char *presult;

    if(strstr(&resp[2],"200 OK"))//搜索200 OK 表示报文正确，进入if
    {                                	
        u3_printf("onenet http 200 ok\r\n");
        u3_printf("onenet_recive_msg_paser..\r\n");//串口提示数据
        if(strstr(&resp[2],"\"errno\":0"))//搜索"errno":0 表示数据流操作正确，进入if
        {                       
            if(strstr(&resp[2],"datastreams"))//搜索 datastreams 表示获取的数据流，是GET报文 查询开关状态
            {                   
                presult = strstr(&resp[2],"\"id\":\"switch_1\""); //搜索 "id":"switch_1" 查询开关1状态
                if(presult!=NULL)//如果搜索到了，进入if
                {                                  
                    if(*(presult-4) == '0')//如果是0，关闭LED0 反之是1，打开LED0
                        drv_led_off(led0);                                  							
                    else 
                        drv_led_on(led0);     
                }
            }
        }
        u3_printf("onenet_recive_msg_paser finished\r\n");//串口提示数据
    } 
    else {
        u3_printf("onenet recive baowen error!\r\n");//串口提示数据
        u3_printf("error baowen data:%s\r\n",&resp[2]);//串口提示数据
    }
}
