#ifndef __USART3_H
#define __USART3_H

#include "stdio.h"
#include "stdarg.h" 
#include "string.h"

#define USART3_RX_ENABLE     1
#define USART3_TXBUFF_SIZE   256

#if  USART3_RX_ENABLE                          
#define USART3_RXBUFF_SIZE   256               
extern char Usart3_RxCompleted ;//Rx-->receive               
extern unsigned int Usart3_RxCounter;          
extern char Usart3_RxBuff[USART3_RXBUFF_SIZE]; 
#endif
void usart3_init(int bound);
void u3_printf(char*,...);         
void usart3_receive_msg_handler(char *receive_str);
#endif


