#include "stm32f4xx.h"  
#include "uart4.h"     
#include "onenet_http.h"     
#include "wifi.h"     

#if  USART4_RX_ENABLE                   
char Usart4_RxCompleted = 0;             
unsigned int Usart4_RxCounter = 0;      
char Usart4_RxBuff[USART4_RXBUFF_SIZE];    	
#endif

UART_HandleTypeDef UART4_Handler;
void Usart4_Init(unsigned int bound)
{ 
    UART4_Handler.Instance = UART4;
    UART4_Handler.Init.BaudRate = bound;
    UART4_Handler.Init.WordLength = UART_WORDLENGTH_8B;
    UART4_Handler.Init.StopBits = UART_STOPBITS_1;
    UART4_Handler.Init.Parity = UART_PARITY_NONE;
    UART4_Handler.Init.Mode = UART_MODE_TX_RX;
    HAL_UART_Init(&UART4_Handler);					    
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    __HAL_RCC_UART4_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**UART4 GPIO Configuration
      PA0-WKUP     ------> UART4_TX
      PA1     ------> UART4_RX
      */
    GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_UART4;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(UART4_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(UART4_IRQn);

    UART4->CR1|=1<<3;  	
    UART4->CR1|=1<<2;  	
    UART4->CR1|=1<<5;    	
    UART4->CR1|=1<<13;  
}

char USART4_TxBuff[USART4_TXBUFF_SIZE];  
//__align(8) char USART4_TxBuff[USART4_TXBUFF_SIZE];  

void u4_printf(char* fmt,...) 
{  
    unsigned int i,length;

    va_list ap;
    va_start(ap,fmt);
    vsprintf(USART4_TxBuff,fmt,ap);
    va_end(ap);	

    length=strlen((const char*)USART4_TxBuff);		
    while((UART4->SR&0X40)==0);
    for(i = 0;i < length;i ++)
    {			
        UART4->DR = USART4_TxBuff[i];
        while((UART4->SR&0X40)==0);	
    }	
}

void UART4_IRQHandler(void)   
{                      
    if((__HAL_UART_GET_FLAG(&UART4_Handler,UART_FLAG_RXNE)!=RESET)) { 
        //if (Connect_flag == 0) {
            if(UART4->DR){                                     	
                Usart4_RxBuff[Usart4_RxCounter]=UART4->DR;     	
                Usart4_RxCounter ++;                             
                Usart4_RxBuff[Usart4_RxCounter]='\0';     	
            }		
        //}
    }
    else if (__HAL_UART_GET_FLAG(&UART4_Handler, UART_FLAG_IDLE)) {
        if (Connect_flag == 1) {//Connect_flag is necessary, because is a qufen flag of esp8266_cmd_response and toucuan_data_response
            //u3_printf("reciceve data\r\n");                               
            Usart4_RxCompleted = 1;                                      
            memcpy(&RXbuff[2],Usart4_RxBuff,Usart4_RxCounter);         
            RXbuff[0] = WiFi_RxCounter/256;                           	
            RXbuff[1] = WiFi_RxCounter%256;                           
            RXbuff[WiFi_RxCounter+2] = '\0';                              
            WiFi_RxCounter=0;                                             
        }
        __HAL_UART_CLEAR_IDLEFLAG(&UART4_Handler);//must clear
    }
} 
