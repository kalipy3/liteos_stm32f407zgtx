#ifndef __USART4_H
#define __USART4_H

#include "stdio.h"      
#include "stdarg.h"		 
#include "string.h"     

#define USART4_RX_ENABLE     1      
#define USART4_TXBUFF_SIZE   1024   

#if  USART4_RX_ENABLE                          
#define USART4_RXBUFF_SIZE   1024              
extern char Usart4_RxCompleted ;               
extern unsigned int Usart4_RxCounter;          
extern char Usart4_RxBuff[USART4_RXBUFF_SIZE]; 
#endif

void Usart4_Init(unsigned int);       
void u4_printf(char*,...) ;          

#endif


