/*
 * picture.c
 * Copyright (C) 2020 2020-12-19 19:42 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#include "picture.h"
#include "node_http.h"
#include "nodeserver_task.h"

//picture usart receive msg handler
void drv_picture_usart_receive_msg_handler(char * receive_str)
{
    if(strcmp("take a picture",receive_str)==0)
    {
        //Take a picture with your Android phone of remote by esp8266
        //why not the camera module of STM32,because I don't have enough money,so I chose to use ADB to call android's camera indirectly through wifi
        nodeserver_doGet_msg_recive_and_paser_task_start();//start a liteos task
    }
}

