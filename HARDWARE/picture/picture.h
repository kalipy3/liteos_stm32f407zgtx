/*
 * picture.h
 * Copyright (C) 2020 kalipy <kalipy@debian>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef PICTURE_H
#define PICTURE_H

//picture usart receive msg handler
void drv_picture_usart_receive_msg_handler(char * receive_str);

#endif /* !PICTURE_H */
